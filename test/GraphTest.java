import org.junit.jupiter.api.Test;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.Interval;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

class GraphTest {
    Graph graph=new Graph(YahooFinance.get("AMZN").getHistory(Interval.DAILY));

    GraphTest() throws IOException {
    }

    @Test
    void getHistory() throws IOException {
    assertEquals(graph.getHistory(),YahooFinance.get("AMZN").getHistory(Interval.DAILY));
    }

    @Test
    void getStringDate() throws IOException {
        assertEquals(Graph.getStringDate(graph.getHistory().get(0)), new SimpleDateFormat("yyyy-MM-dd").format(YahooFinance.get("AMZN").getHistory(Interval.DAILY).get(0).getDate().getTime()));
    }

    @Test
    void getAverage() throws IOException {
        assertEquals(Graph.getAverage(graph.getHistory().get(0)),
                graph.getHistory().get(0).getOpen().add(
                        graph.getHistory().get(0).getHigh().add(
                                graph.getHistory().get(0).getLow().add(
                                        graph.getHistory().get(0).getClose()))).divide(
                                                new BigDecimal(4)));
    }

    @Test
    void getMovingAverage() throws IOException {
        assertEquals(graph.getMovingAverage(4).get(2),
                (Graph.getAverage(graph.getHistory().get(0)).doubleValue()+
                        Graph.getAverage(graph.getHistory().get(1)).doubleValue()+
                        Graph.getAverage(graph.getHistory().get(2)).doubleValue()+
                        Graph.getAverage(graph.getHistory().get(3)).doubleValue())/4);
    }

    @Test
    void getWeightedMovingAverage() throws IOException {

        assertEquals(graph.getWeightedMovingAverage(4).get(2),
                (Graph.getAverage(graph.getHistory().get(0)).doubleValue()*1+
                        Graph.getAverage(graph.getHistory().get(1)).doubleValue()*2+
                        Graph.getAverage(graph.getHistory().get(2)).doubleValue()*3+
                        Graph.getAverage(graph.getHistory().get(3)).doubleValue()*4)/10);
    }

    @Test
    void getDailyIncreaseRatio() throws IOException {
    }

    @Test
    void getGain() {
    }

    @Test
    void getMaxGain() {
    }
}