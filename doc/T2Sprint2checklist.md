# T2 Sprint 2 checklist

* Documentazione di progettazione aggiornata: PARTIAL (UML mancante, *ancora*); 
* Sprint backlog aggiornato su trello: PARTIAL - NON MODIFICATO E SENZA SPRINT BACKLOGS
* Codice : YES (Not much though)
* Codice JUnit : NO
* JavaDoc: NO
* Company logo: No
* Documentazione in formato gittabile (grafici esclusi) : NO (bonus)
* Diario giornaliero delle attività personali (incluse ore fuori orario) : NO (bonus)
* Daily status: PARTIAL (missing wed) 
* Review video: OK (a little late)
* Retrospettiva : NO


