# T1 Sprint 1 checklist

* Documentazione di progettazione aggiornata: PARTIAL (UML mancante, ancora); 
* Sprint backlog aggiornato su trello: OK (un problema sugli Sprint backlog)
* Codice : NO !! 
* Codice JUnit : NO
* JavaDoc: NO
* Documentazione in formato gittabile (grafici esclusi) : NO (bonus)
* Diario giornaliero delle attività personali (incluse ore fuori orario) : NO (bonus)
* Daily status: YES 
* Review video: YES (some problems)
* Retrospettiva : NO


