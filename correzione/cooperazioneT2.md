# TEAMWORK

## Impegno
Ruoli ufficiali (ore dichiarate, log presente ma non misurato e personalizzato): 
-Tester (Marco Dondi)
-Documenter (Lorenzo Pecchi)
-Interazione with customer (Alessandro Pietrafitta)
-HTML/CSS Developer (Marco Dondi)
-Java Developers (Denis Cremonese & Lorenzo Pecchi & Cristian Facchini)
-Program Structure (Alessandro Pietrafitta & Denis Cremonese)
 



## Git 
    39  marco dondi (index.html, Day_forDay.txt, README.md)
    30  Lorenzo Pecchi (DayForDay.txt, README.md, 
     13 Denis Cremonese (index.html, index.html, Main.java, Graph.java, GraphTest.java, FileContentBuffer.java)
     2  Alessandro  Pietrafitta 
     1  Cristian Facchini 
     


## Retrospective
Prima retrospettiva eseguita. Azioni troppo nebulose
Seconda retrospettiva non eseguita


##Sintesi

Emerge un gruppo che ha lavorato in modo piuttosto disunito. Di fatto, il grosso del lavoro si concentra su tre individui che si sono spartiti le cose da fare in modo "istintivo", senza troppa coordinazione - gli altri hanno fatto poco. La pianificazione è stata poca e/o ignorata, il metodo suggerito pure. Il risultato è stato dovuto alle indubbie capacità individuali e poco più. 
Molti si lamentano della "cattiveria" del cliente/datore di lavoro e dell' "insistenza" per richiedere "documentazione inutile". Putroppo, non siete voi a decidere cosa è utile o inutile quando lavorate per altri, e in ogni caso molta gente invece di guardare dei film poteva occuparsi della documentazione. Il progetto è concepito per far lavorare 5 persone, non necessariamente tutti programmatori. Altri gruppi sono riusciti a sfruttare le potenzialità dei singoli, voi no. 


