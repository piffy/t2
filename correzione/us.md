User stories totali (fatte) 10/13 (trello)
Commento US: Generalmente scarisine. Manca qualsiasi riferimento al fatto che l'input derivi dalla chiamate rest al server; le US non sono mai state aggiornate - per quanto si può dedurre dal log di trello.Stimate: no, Prioritizzate: non si capisce
Organizzazione in sprint (velocity): no -   media 2.5 US al giorno

CRITICA
BACKLOG
As a user, I need to be able to choose the stock market title in a list
	NOT IN THE REQUIREMENTS
As a user, i need to be able to select a time range of the selected index, on a specific date
	NOT IN THE REQUIREMENTS
As a user, I need to be able to make a comparison between two indexes
	UNCLEAR. REALLY
As a user I need to be able to see the time slot for when the market is open
	NOT IN THE REQUIREMENTS


Sprint 2 As user I want to see the page
	NOT REALLY CLEAR
Sprint 4 As an administrator i need to be able to edit the market stock selected
	NOT IN THE REQUIREMENTS
Sprint 5 As a user I need to be able to read the data via line chart, candlestick chart
	OK
We need to generate graphs
	UNDEFINED
String 6 As a user, i need to be able to read information about the relevant stock exchange
	NOT TOO CLEAR. AT BEST IT COULD BE BROKEN DOWN IN SEVERAL STORIES
We need to have information for each stock on the list
	NOT IN THE REQUIREMENTS
Description of the company
	BADLY WRITTEN
As a user i have to be able to view an image representing the company
	OK
String 6 As a user, i want to be provided the most recent data
	NOT TOO CLEAR
We need to show information about the index changes in real time
	OK, BUT HOW? THIS IS TOO VAGUE
