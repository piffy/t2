import Jwiki.Jwiki;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

public class Main {
    public static String ticker;
    public static int period=4;


    public static void main(String[] args) throws IOException, InterruptedException {
        Graph graph;
        String[] datas;
        GregorianCalendar startingDate, endingDate;
        FileContentBuffer index=new FileContentBuffer("index.html");
        while (true){
            datas=getUrlContents("https://dwweb.gnet.it/dw2022/").split("\"");
        ticker=datas[3];
        startingDate=getCalendarByString(datas[7]);
        endingDate=getCalendarByString(datas[11]);

        graph=new Graph(YahooFinance.get(ticker).getHistory(startingDate,endingDate,Interval.DAILY));
        //graph=new Graph(YahooFinance.get("AMZN").getHistory(new GregorianCalendar(2014,1,1),Interval.DAILY)); ticker=graph.getHistory().get(0).getSymbol();

        write(index,graph);
        Thread.sleep(30000);
        }}

    /**
     * Writes in the HTML file the graph data.
     *
     * @param index the HTML file contents.
     * @param graph the graph data.
     * @throws IOException
     */
    private static void write(FileContentBuffer index, Graph graph) throws IOException
    {        TreeMap<Integer,Integer> valuesFrequency= new TreeMap<>();
        Jwiki description= new Jwiki(YahooFinance.get(ticker).getName());
        String dates="", values="", o="", h="", l="", c="", x="", name=YahooFinance.get(ticker).getName();
        for (HistoricalQuote hq : graph.getHistory())
    {dates+="'"+Graph.getStringDate(hq)+"',";
        values+=Graph.getAverage(hq)+",";
        Integer average=Graph.getAverage(hq).intValue();
        if(!valuesFrequency.containsKey(average)) valuesFrequency.put(average,1);
        else valuesFrequency.put(average, valuesFrequency.get(average)+1);
        x+=getLuxonDate(hq.getDate())+ ",";
        o+=hq.getOpen()+",";
        h+=hq.getHigh()+",";
        l+=hq.getLow()+",";
        c+=hq.getClose()+",";
    }


        replaceFileLine(index,53,"                <img src=\""+description.getImageURL() +"\" alt=\"\">");
        replaceFileLine(index,56,"        <h3 class=\"big-text\">"+description.getDisplayTitle()+ "</h3>");
        replaceFileLine(index,57,"<p>"+description.getExtractText()+"</p>");
        replaceFileLine(index,62,"    <div><font color=\"#C80000\">Ticker:</font> "+ticker+"&emsp;&emsp;&emsp;<font color=\"#C80000\">Max Possible Gain:</font> "+graph.getMaxGain()+"&emsp;&emsp;&emsp;<font color=\"#C80000\">Gain: </font>"+graph.getGain()+"&emsp;&emsp;&emsp;<font color=\"#0AC800\">("+(float)graph.getDailyIncreaseRatio()+"%)"+"</font>&emsp;&emsp;&emsp;<font color=\"#C80000\">Market: </font>"+YahooFinance.get(ticker).getStockExchange()+"</div>");
        replaceFileLine(index,70,"    let values=["+values+"];");
        replaceFileLine(index,71,"    let dates=["+dates+"];");
        replaceFileLine(index,77,"            label: '"+name+"',");
        replaceFileLine(index,87,"            label: '"+name+" Moving Average',");
        replaceFileLine(index,88,"            data: "+graph.getMovingAverage(period).toString()+",");
        replaceFileLine(index,100,"            label: '"+name+" Weighted Moving Average',");
        replaceFileLine(index,101,"            data: "+graph.getWeightedMovingAverage(period).toString()+",");
        replaceFileLine(index,139,"        let frequency="+valuesFrequency.values());
        replaceFileLine(index,140,"        let repetitiveValues="+valuesFrequency.keySet());
        replaceFileLine(index,146,"                        label: '"+name+" values frequency',");
        replaceFileLine(index,185,"                        const date =["+x+"];");
        replaceFileLine(index,187,"                        o =["+o+"];");
        replaceFileLine(index,188,"                        h =["+h+"];");
        replaceFileLine(index,189,"                        l =["+l+"];");
        replaceFileLine(index,190,"                        c =["+c+"];");
        replaceFileLine(index,192,"    for(var i=0; i<"+ graph.getHistory().size()+"; i++)");
        replaceFileLine(index,1205,"            label: '"+name+"',");
        index.write();}

    /**
     * Converts a dd/MM/yyyy type date into a date object
     *
     * @param date the dd/MM/yyyy date.
     * @return a calendar object containing the string data.
     */
    private static GregorianCalendar getCalendarByString(String date)
        {String[] tempDates=date.split("/");
            return new GregorianCalendar(Integer.parseInt(tempDates[2]),Integer.parseInt(tempDates[1]),Integer.parseInt(tempDates[0]));}

    /**
     * Translates the date into a compatible format for the Luxon library.
     *
     * @param date the date to format.
     * @return the Luxon compatible date.
     */
    private static String getLuxonDate(Calendar date)
        {String monthName="";
        switch (date.get(Calendar.MONTH))
            {
                case 0: monthName = "Jan";break;
                case 1: monthName = "Feb";break;
                case 2: monthName = "Mar";break;
                case 3: monthName = "Apr";break;
                case 4: monthName = "May";break;
                case 5: monthName = "Jun";break;
                case 6: monthName = "Jul";break;
                case 7: monthName = "Aug";break;
                case 8: monthName = "Sep";break;
                case 9: monthName = "Oct";break;
                case 10: monthName = "Nov";break;
                case 11: monthName = "Dec";break;
            }
            return "luxon.DateTime.fromRFC2822('"+ date.get(Calendar.DAY_OF_MONTH)+" "+monthName+" "+date.get(Calendar.YEAR)+" 00:00 GMT')";
        }

    /**
     * Gets the entire HTML contents through a given URL.
     *
     * @param theUrl the webpage URL.
     * @return the webpage's HTML.
     */
    private static String getUrlContents(String theUrl)
    {
        StringBuilder content = new StringBuilder();
        // Use try and catch to avoid the exceptions
        try
        {
            URL url = new URL(theUrl); // creating a url object
            URLConnection urlConnection = url.openConnection(); // creating a urlconnection object

            // wrapping the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;

            // reading from the urlconnection using the bufferedreader
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }

    /**
     * Replaces line at indexLine with line newLine.
     *
     * @param fileContentBuffer the file contents.
     * @param indexLine the index of the line you want to change.
     * @param newLine the new line that you input.
     * @throws IOException
     */
    public static void replaceFileLine(FileContentBuffer fileContentBuffer, int indexLine, String newLine) throws IOException {
        BufferedReader br
                     = new BufferedReader(new StringReader(fileContentBuffer.getContent()));
            String line, newContent="";
            int i=1;
            while ((line = br.readLine()) != null) {
                if(i==indexLine)
                    newContent+=newLine;
                else
                    newContent+=line;
                newContent+="\n";
                i++;
            }
            fileContentBuffer.setContent(newContent);
    }

}

