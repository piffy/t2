import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileContentBuffer {
    private String content, path;

    /**
     *Gets the contents of a file and saves them as a string.
     *
     * @param path the path of your chosen file.
     * @throws IOException
     */
    public FileContentBuffer(String path) throws IOException {
        this.path=path;
        StringBuilder stringBuilder = new StringBuilder();
            try (BufferedReader br
                         = new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream(path)))) {
                String line;
                while ((line = br.readLine()) != null) {
                        stringBuilder.append(line);
                    stringBuilder.append("\n");
                }
        }
    content =stringBuilder.toString();
    }

    /**
     * Returns the file content.
     *
     * @return the file content.
     */
    public String getContent() {
        return content;
    }

    /**
     * Set the file content.
     *
     * @param content the new content.
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Writes the content in the file.
     *
     * @throws IOException
     */
    public void write() throws IOException {
            FileWriter writer=new FileWriter("site\\"+path);
            writer.write(content);
            writer.close();
        }
}
