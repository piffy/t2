import yahoofinance.histquotes.HistoricalQuote;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Graph {
    private List<HistoricalQuote> history;


    public Graph(List<HistoricalQuote> history) {
            this.history=history;
        }

    public List<HistoricalQuote> getHistory() {
        return history;
    }

    public static String getStringDate(HistoricalQuote historicalQuote)
        { return new SimpleDateFormat("yyyy-MM-dd").format(historicalQuote.getDate().getTime());}

    /**
     * Calculates the average between the Open and Close and the High and Low of an HistoricalQuote.
     *
     * @param hq the HistoricalQuote you want to calculate the average of.
     * @return the average between the Open and Close and the High and Low of the HistoricalQuote.
     */
    public static BigDecimal getAverage(HistoricalQuote hq)
        {return hq.getOpen().add(hq.getHigh().add(hq.getClose().add(hq.getLow()))).divide(new BigDecimal(4));}

    /**
     * Calculates the moving average of the graph data in a specific time slot.
     *
     * @param period the time period you want to calculate the moving average of.
     * @return the moving average
     */
    public List<Double> getMovingAverage(int period)
        {List<Double> t=new ArrayList<>();
            for(int i=0; i<history.size()-(period/2); i++) {
                if (i < period / 2)
                    t.add(history.get(0).getLow().doubleValue());
                else {
                    Double average = 0.0;
                    for (int j = i - period / 2; j < i + period / 2; j++)
                        average += Graph.getAverage(history.get(j)).doubleValue();
                    t.add(average/period);
                }
            }
            return t;
        }

    /**
     * Calculates the weighted moving average of the graph data in a specific time slot.
     *
     * @param period the time period you want to calculate the moving average of.
     * @return the weighted moving average
     */
    public List<Double> getWeightedMovingAverage(int period)
    {List<Double> t=new ArrayList<>();
        int sum=0;
        for (int i=1; i<=period; i++)
            sum+=i;
        for(int i=0; i<history.size()-(period/2); i++) {
            if (i < period / 2)
                t.add(history.get(0).getLow().doubleValue());
            else {
                Double average = 0.0;
                int k=1;
                for (int j = i - period / 2; j < i + period / 2; j++)
                { average += Graph.getAverage(history.get(j)).doubleValue()*k;
                k++;}

                t.add(average/sum);
            }
        }
        return t;
    }

    /**
     * Calculates the percentage of days where the market closed higher than it opened.
     *
     * @return the percentage.
     */
    public double getDailyIncreaseRatio()
        {int increaseDays=0;
            for (HistoricalQuote hq : history)
            {if(hq.getClose().compareTo(hq.getOpen())==1)
                increaseDays++;}
            return ((double)increaseDays/(double)history.size()*100);
        }

        public BigDecimal getGain()
        { return history.get(history.size()-1).getClose().subtract(history.get(0).getOpen());}

    /**
     * Calculates the maximum possible gain if you always bought at the lowest point and sold at the highest each day.
     *
     * @return the maximum possible gain.
     */
    public BigDecimal getMaxGain()
        {BigDecimal gain=new BigDecimal(0);
            for (HistoricalQuote hq : history)
                gain=gain.add(hq.getHigh().subtract(hq.getLow()));
            return gain;
        }
}
